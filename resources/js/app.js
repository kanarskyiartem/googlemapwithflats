require('./bootstrap');
import Vue from "vue";
import * as VueGoogleMaps from 'vue2-google-maps';

window.Vue = require('vue').default;

// Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.use(VueGoogleMaps, {
    load: {
        key: 'AIzaSyDXZgMjc9GbrabN6sYsQIVkBduqSUk_2-c',
    }
});

const app = new Vue({
    el: '#app',
    data() {
        return {
            flats: [],
            infoFlatOptions: {
                pixelOffset: {
                    width: 0,
                    height: -35
                }
            },
            activeFlat: {},
            infoWindowFlatOpened: false
        }
    },
    created() {
        axios.get('/api/flats')
            .then(response => this.flats = response.data)
            .catch((error) => console.error(error))
    },
    methods: {
        handleMarkerClicker(flat) {
            this.activeFlat = flat;
            this.infoWindowFlatOpened = true;
        },
        handleWindowClose() {
            this.activeFlat = {};
            this.infoWindowFlatOpened = false;
        }
    },
    computed: {
        infoWindowPosition() {
            return {
                lat: parseFloat(this.activeFlat.latitude),
                lng: parseFloat(this.activeFlat.longitude)
            }
        }
    }
});
