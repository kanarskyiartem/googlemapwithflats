<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class FlatFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'address' => $this->faker->address(),
            'photo' => $this->faker->imageUrl(640,480),
            'description' => $this->faker->text(150),
            'price' => $this->faker->randomFloat(2,5000,20000), // password
            'latitude' => $this->faker->latitude(50.46, 50.47),
            'longitude' => $this->faker->longitude(30.51, 30.52),
        ];
    }

}
